self.addEventListener('install', event => {
    console.log('V1 installing…');
    // cache a cat SVG
    event.waitUntil(
        caches.open('static-v1').then(cache => cache.add('/img/d8447f6ajw1ei6rxca9iwj21kw0w07in.jpg'))
    );
});

self.addEventListener('activate', event => {
    console.log('V1 now ready to handle fetches!');
});

self.addEventListener('fetch', event => {
    const url = new URL(event.request.url);

    // serve the cat SVG from the cache if the request is
    // same-origin and the path is '/dog.svg'
    if (url.origin == location.origin && url.pathname.endsWith('005wh0VEgy1fjlg81bddsj31w01tjnpe.jpg')) {
        event.respondWith(caches.match('d8447f6ajw1ei6rxca9iwj21kw0w07in.jpg'));
    }
});